package mutations

import (
	"server/repository"
	"server/types"

	log "github.com/sirupsen/logrus"
	"github.com/satori/go.uuid"

	"github.com/graphql-go/graphql"
)

// CreateListsOpinions creates a new lists opinions and returns it.
func CreateListsOpinions() *graphql.Field {
	return &graphql.Field{
		Type: types.ListsOpinionsType,
		Args: graphql.FieldConfigArgument{
			"idOpinion": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"idUser": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"idProject": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[mutation] create list opinion\n")
			log.WithFields(log.Fields{
				"idOpinion": params.Args["idOpinion"].(string),
				"idUser":    params.Args["idUser"].(string),
				"idProject": params.Args["idProject"].(string),
			}).Debug("__ create list opinion\n")

			idOpinion, _ := uuid.FromString(params.Args["idOpinion"].(string))
			idUser, _ := uuid.FromString(params.Args["idUser"].(string))
			idProject, _ := uuid.FromString(params.Args["idProject"].(string))

			listsOpinions := &types.ListsOpinions{
				ID_opinion:	idOpinion,
				ID_user:	idUser,
				ID_project:	idProject,
			}

			repository.Create(listsOpinions)

			return listsOpinions, nil
		},
	}
}
