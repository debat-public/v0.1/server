package mutations

import (
	"server/repository"
	"server/types"
	"server/utils"

	log "github.com/sirupsen/logrus"
	"github.com/satori/go.uuid"

	"github.com/graphql-go/graphql"
)

// CreateProject creates a new project and returns it.
func CreateProject() *graphql.Field {
	return &graphql.Field{
		Type: types.ProjectType,
		Description: "return new project, create relationship with user and list of block",
		Args: graphql.FieldConfigArgument{
			"title": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"description": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"idUser" : &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"listBlocks" : &graphql.ArgumentConfig{
				Type: graphql.NewList(types.BlockInputType),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[mutation] create project\n")
			log.WithFields(log.Fields{
				"title": params.Args["title"].(string),
				"description": params.Args["description"].(string),
				"idUser": params.Args["idUser"].(string),
				}).Debug("__ create project\n")

			project := &types.Project{
				Title: params.Args["title"].(string),
				Description: params.Args["description"].(string),
				CreatedAt: utils.GetDate(),
			}

			repository.Create(project)

			// Create relationship between new project and the user
			idUser, _ := uuid.FromString(params.Args["idUser"].(string))

			listsProjects := &types.ListsProject{
				ID_user:  idUser,
				ID_project: project.ID,
			}

			log.WithFields(log.Fields{
				"idProject":   project.ID,
			}).Debug("__ create project __ before create list project\n")

			repository.Create(listsProjects)

			// Create relationship between new project and list of block

			var listsBlocks *types.ListsBlocks
			var block *types.Block

			for _, e := range params.Args["listBlocks"].([] interface {}) {
				tmp := e.(map[string]interface {})

				block = &types.Block{
					Title: tmp["title"].(string),
					Body:  tmp["body"].(string),
					Index:  tmp["index"].(int),
				}
				repository.Create(block)

				listsBlocks = &types.ListsBlocks{
					ID_block:   block.ID,
					ID_project: project.ID,
				}

				log.WithFields(log.Fields{
					"idBlock": block.ID,
					"title": block.Title,
					"body": block.Body,
				}).Debug("__ create block __ before create list block\n")

				repository.Create(listsBlocks)
			}

			return project, nil
		},
	}
}