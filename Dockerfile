FROM golang:1.13

# GET ENVIRONMENT (debug, release) PASSED AS A COMMAND-LINE ARGUMENT
ARG ENV

# DEFINE ENVIRONNEMENT VARIABLES
# ENV APP=${ENV}

RUN go get golang.org/x/crypto/bcrypt
# RUN  apk update && apk upgrade && apk add watch

# SET WORKING DIRECTORY
WORKDIR /go/src/app

# ADD PROJECT
COPY . .

# BUILD APP
RUN make build

# BUILD GODOC Documentation
# TODO ADD GODOC

# TODO ADD GO TEST


# EXPOSE PORT 8383
EXPOSE 8383

# START THE APPLICATION
# ENTRYPOINT ["make", "run"]
CMD ["make", "run", "log_level=info"]