package mutations

import (
	"server/repository"
	"server/types"

	log "github.com/sirupsen/logrus"
	"github.com/satori/go.uuid"

	"github.com/graphql-go/graphql"
)

// CreateListsProjects creates a new lists writers and returns it.
func CreateListsProjects() *graphql.Field {
	return &graphql.Field{
		Type: types.ListsProjectType,
		Args: graphql.FieldConfigArgument{
			"idProject": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"idWriter": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[mutation] create list writer\n")
			log.WithFields(log.Fields{
				"idWriter":  params.Args["idWriter"].(string),
				"idProject": params.Args["idProject"].(string),
			}).Debug("__ create list writer\n")
			idWriter, _ := uuid.FromString(params.Args["idWriter"].(string))
			idProject, _ := uuid.FromString(params.Args["idProject"].(string))

			log.WithFields(log.Fields{
				"idWriter":  idWriter,
				"idProject": idProject,
			}).Debug("__ create list writer\n")

			listsProjects := &types.ListsProject{
				ID_user:  idWriter,
				ID_project: idProject,
			}

			repository.Create(listsProjects)

			return listsProjects, nil
		},
	}
}
