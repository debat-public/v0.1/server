package queries

import (
	log "github.com/sirupsen/logrus"

	"server/repository"
	"server/types"

	"github.com/satori/go.uuid"
	"github.com/graphql-go/graphql"
)

type ListDataOpinion struct {
	Project		types.Project
	User		types.User
	Opinion		types.Opinion
}

// ListDataOpinionType is an output GraphQL schema for the opinion user block relation type.
var ListDataOpinionType = graphql.NewObject(graphql.ObjectConfig{
	Name: "ListDataOpinion",
	Fields: graphql.Fields{
		"opinion": &graphql.Field{Type: types.OpinionType},
		"user":   &graphql.Field{Type: types.UserType},
		"project": &graphql.Field{Type: types.ProjectType},
	},
})

// FindOpinion returns opinion from idOpinion.
func FindOpinion() *graphql.Field {
	return &graphql.Field{
		Type:        ListDataOpinionType,
		Description: "Return Opinion from ID",
		Args: graphql.FieldConfigArgument{
			"id": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[query] Opinion \n")
			log.WithFields(log.Fields{
				"id": params.Args["id"].(string),
			}).Debug("__ Query Opinion\n")

			var opinion types.Opinion
			var project types.Project
			var user types.User
			var listsOpinions []types.ListsOpinions

			id, _ := uuid.FromString(params.Args["id"].(string))

			repository.Find(&opinion, id)

			repository.FindListBy(&listsOpinions, "id_opinion", id, -1, -1)

			repository.Find(&project, listsOpinions[0].ID_project)
			repository.Find(&user, listsOpinions[0].ID_user)

			dataProject := &ListDataOpinion{
				Project : project,
				User : user,
				Opinion : opinion,
			}

			return dataProject, nil
		},
	}
}

// FindListOpinions returns the queries available against project type.
func FindListOpinions() *graphql.Field {
	return &graphql.Field{
		Type: graphql.NewList(types.ProjectType),
		Args: graphql.FieldConfigArgument{
			"offset": &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: 0,
			},
			"limit": &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: 0,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[query] GetListOpinionsQuery\n")
			log.WithFields(log.Fields{
				"offset": params.Args["offset"].(int),
				"limit":  params.Args["limit"].(int),
			}).Debug("__ Query ListOpinions\n")

			var opinions []types.Opinion

			repository.FindAll(&opinions, params.Args["offset"].(int), params.Args["limit"].(int))

			return opinions, nil
		},
	}
}