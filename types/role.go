package types

import (
	"github.com/graphql-go/graphql"
	"github.com/satori/go.uuid"
	"github.com/jinzhu/gorm"
)

// Role type definition.
type Role struct {
	ID   uuid.UUID	`gorm:"type:uuid;primary_key;" db:"id" json:"id"`
	Name string `db:"name" json:"name"`
}

// RoleType is the GraphQL schema for the user type.
var RoleType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Role",
	Fields: graphql.Fields{
		"id":   &graphql.Field{Type: graphql.String},
		"name": &graphql.Field{Type: graphql.String},
	},
})

// BeforeCreate will set a UUID rather than numeric ID.
func (role *Role) BeforeCreate(scope *gorm.Scope) error {
	uuid := uuid.NewV4()
	return scope.SetColumn("ID", uuid)
}