package utils

import (
	"encoding/json"
	"fmt"
	"server/types"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

// Credential type
type Credential struct {
	Host     string `json:"host"`
	Port     string `json:"port"`
	User     string `json:"user"`
	DBname   string `json:"db_name"`
	Password string `json:"password"`
}

var DB *gorm.DB

func InitPostgresSQL() {
	fmt.Println("Init postgres ...")
	var credential Credential
	var err error
	var byteValue []byte
	fmt.Println("After var ...")
	byteValue, err = ReadJSON("/json/credential.json")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("After readjson ...")
	json.Unmarshal(byteValue, &credential)
	fmt.Println("After unsmarshall ...")
	DB, err = gorm.Open("postgres", "host="+credential.Host+" port="+credential.Port+" user="+credential.User+" dbname="+credential.DBname+" password="+credential.Password+" sslmode=disable")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Before db close")
	fmt.Println("User ...")
	DB.AutoMigrate(&types.User{})
	fmt.Println("Role ...")
	DB.AutoMigrate(&types.Role{})
	fmt.Println("Project ...")
	DB.AutoMigrate(&types.Project{})
	fmt.Println("Opinion ...")
	DB.AutoMigrate(&types.Opinion{})
	fmt.Println("Reaction ...")
	DB.AutoMigrate(&types.Reaction{})
	fmt.Println("Block ...")
	DB.AutoMigrate(&types.Block{})
	fmt.Println("ListOpinion ...")
	DB.AutoMigrate(&types.ListsOpinions{})
	fmt.Println("ListReaction ...")
	DB.AutoMigrate(&types.ListsReactions{})
	fmt.Println("ListUsers ...")
	DB.AutoMigrate(&types.ListUsers{})
	fmt.Println("ListBlock ...")
	DB.AutoMigrate(&types.ListsBlocks{})
	fmt.Println("ListProject ...")
	DB.AutoMigrate(&types.ListsProject{})
	fmt.Println("End postgres ...")
}
