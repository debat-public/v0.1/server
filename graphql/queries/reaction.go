package queries

import (
	log "github.com/sirupsen/logrus"

	"server/repository"
	"server/types"

	"github.com/satori/go.uuid"
	"github.com/graphql-go/graphql"
)

type ListDataReaction struct {
	Project		types.Project
	User		types.User
	Reaction	types.Reaction
}

// ListDataReactionType is an output GraphQL schema for the reaction user block relation type.
var ListDataReactionType = graphql.NewObject(graphql.ObjectConfig{
	Name: "ListDataReaction",
	Fields: graphql.Fields{
		"reaction": &graphql.Field{Type: types.ReactionType},
		"user":   &graphql.Field{Type: types.UserType},
		"project": &graphql.Field{Type: types.ProjectType},
	},
})

// FindReaction returns reaction from idReaction.
func FindReaction() *graphql.Field {
	return &graphql.Field{
		Type:        ListDataReactionType,
		Description: "Return Reaction from ID",
		Args: graphql.FieldConfigArgument{
			"id": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[query] Reaction \n")
			log.WithFields(log.Fields{
				"id": params.Args["id"].(string),
			}).Debug("__ Query Reaction\n")

			var reaction types.Reaction
			var project types.Project
			var user types.User
			var listsReactions []types.ListsReactions

			id, _ := uuid.FromString(params.Args["id"].(string))

			repository.Find(&reaction, id)

			repository.FindListBy(&listsReactions, "id_reaction", id, -1, -1)

			repository.Find(&project, listsReactions[0].ID_project)
			repository.Find(&user, listsReactions[0].ID_user)

			dataProject := &ListDataReaction{
				Project : project,
				User : user,
				Reaction : reaction,
			}

			return dataProject, nil
		},
	}
}

// FindListReactions returns the queries available against project type.
func FindListReactions() *graphql.Field {
	return &graphql.Field{
		Type: graphql.NewList(types.ProjectType),
		Args: graphql.FieldConfigArgument{
			"offset": &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: 0,
			},
			"limit": &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: 0,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[query] GetListReactionsQuery\n")
			log.WithFields(log.Fields{
				"offset": params.Args["offset"].(int),
				"limit":  params.Args["limit"].(int),
			}).Debug("__ Query ListReactions\n")

			var reactions []types.Reaction

			repository.FindAll(&reactions, params.Args["offset"].(int), params.Args["limit"].(int))

			return reactions, nil
		},
	}
}