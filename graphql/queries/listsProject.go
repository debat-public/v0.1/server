package queries

import (
	log "github.com/sirupsen/logrus"

	"server/repository"
	"server/types"

	"github.com/satori/go.uuid"
	"github.com/graphql-go/graphql"
)

// FindListProject return list of project from a user
func FindListProject() *graphql.Field {
	return &graphql.Field{
		Type:        graphql.NewList(types.ProjectType),
		Description: "return list of project/user",
		Args: graphql.FieldConfigArgument{
			"id_user": &graphql.ArgumentConfig{
				Type:         graphql.NewNonNull(graphql.String),
			},
			"offset": &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: -1,
			},
			"limit": &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: -1,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[query] FindListProjectBy\n")
			log.WithFields(log.Fields{
				"id":     params.Args["id_user"].(string),
				"offset": params.Args["offset"].(int),
				"limit":  params.Args["limit"].(int),
			}).Debug("__ Query ListsProjectBy\n")

			var ret []types.ListsProject
			var projects []types.Project

			id, _ := uuid.FromString(params.Args["id_user"].(string))

			repository.FindListBy(&ret, "id_user", id, params.Args["offset"].(int), params.Args["limit"].(int))

			listIndex := getIdProject(ret)

			repository.FindByList(&projects, listIndex)

			return projects, nil
		},
	}
}

func getIdProject(source []types.ListsProject) ([]string) {
	new_tab := make([]string, len(source))

	for i, e := range source {
		new_tab[i] = e.ID_project.String()
	}

	return new_tab
}
