package queries

import (
	log "github.com/sirupsen/logrus"

	"server/repository"
	"server/services"
	"server/types"

	"encoding/json"
	"net/http"
	"time"

	"github.com/satori/go.uuid"
	"github.com/graphql-go/graphql"
)

type ListDataUser struct {
	User		types.User
	Project		[]types.Project
	Opinion		[]types.Opinion
	Reaction	[]types.Reaction
}

// ListDataUserType is an output GraphQL schema for the project user block relation type.
var ListDataUserType = graphql.NewObject(graphql.ObjectConfig{
	Name: "ListDataUser",
	Fields: graphql.Fields{
		"user":   &graphql.Field{Type: types.UserType},
		"project":   &graphql.Field{Type: graphql.NewList(types.ProjectType)},
		"opinion":   &graphql.Field{Type: graphql.NewList(types.OpinionType)},
		"reaction":   &graphql.Field{Type: graphql.NewList(types.ReactionType)},
	},
})


// FindUser returns user from id.
func FindUser() *graphql.Field {
	return &graphql.Field{
		Type:        ListDataUserType,
		Description: "Return User from ID",
		Args: graphql.FieldConfigArgument{
			"id": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[query] User \n")
			log.WithFields(log.Fields{
				"id": params.Args["id"].(string),
			}).Debug("__ Query User\n")

			var user types.User
			var listsProject []types.ListsProject
			var listsReactions []types.ListsReactions
			var listsOpinions []types.ListsOpinions
			var projects []types.Project
			var opinions []types.Opinion
			var reactions []types.Reaction

			id, _ := uuid.FromString(params.Args["id"].(string))
			
			repository.Find(&user, id)

			repository.FindListBy(&listsProject, "id_user", id, -1, -1)
			repository.FindListBy(&listsReactions, "id_user", id, -1, -1)
			repository.FindListBy(&listsOpinions, "id_user", id, -1, -1)

			listIndexProjects := getId_Project(listsProject)
			listIndexReaction := getId_Reaction(listsReactions)
			listIndexOpinions := getId_Opinion(listsOpinions)
			
			repository.FindByList(&projects, listIndexProjects)
			repository.FindByList(&opinions, listIndexOpinions)
			repository.FindByList(&reactions, listIndexReaction)

			dataProject := &ListDataUser{
				User : user,
				Project : projects,
				Reaction : reactions,
				Opinion : opinions,
			}

			return dataProject, nil
		},
	}
}

// FindListUsers
func FindListUsers() *graphql.Field {
	return &graphql.Field{
		Type: graphql.NewList(types.UserType),
		Args: graphql.FieldConfigArgument{
			"offset": &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: 0,
			},
			"limit": &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: 0,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[query] Query ListUsers\n")
			log.WithFields(log.Fields{
				"offset": params.Args["offset"].(int),
				"limit":  params.Args["limit"].(int),
			}).Debug("__ Query ListUsers\n")

			var users []types.User

			repository.FindAll(&users, params.Args["offset"].(int), params.Args["limit"].(int))

			return users, nil
		},
	}
}

func getId_Project(source []types.ListsProject) ([]string) {
	new_tab := make([]string, len(source))

	for i, e := range source {
		new_tab[i] = e.ID_project.String()
	}

	return new_tab
}

func getId_Reaction(source []types.ListsReactions) ([]string) {
	new_tab := make([]string, len(source))

	for i, e := range source {
		new_tab[i] = e.ID_reaction.String()
	}

	return new_tab
}

func getId_Opinion(source []types.ListsOpinions) ([]string) {
	new_tab := make([]string, len(source))

	for i, e := range source {
		new_tab[i] = e.ID_opinion.String()
	}

	return new_tab
}

var client = &http.Client{Timeout: 10 * time.Second}

func getJson(url string, target interface{}) error {
	r, err := client.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

func GetProvideUserQuery() *graphql.Field {
	return &graphql.Field{
		Type: types.UserType,
		Args: graphql.FieldConfigArgument{
			"code": &graphql.ArgumentConfig{
				Type:         graphql.String,
				DefaultValue: "",
			},
			"state": &graphql.ArgumentConfig{
				Type:         graphql.String,
				DefaultValue: "",
			},
			"provider": &graphql.ArgumentConfig{
				Type:         graphql.String,
				DefaultValue: "",
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[query] ProvideUser\n")
			var user types.User

			if params.Args["provider"] == "42" {
				services.ConnectWith42(params, &user)
				return user, nil
			}

			return nil, nil
		},
	}
}
