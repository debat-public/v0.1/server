package mutations

import (
	"server/repository"
	"server/types"

	log "github.com/sirupsen/logrus"
	"github.com/satori/go.uuid"

	"github.com/graphql-go/graphql"
)

// CreateListsReactions creates a new lists reactions and returns it.
func CreateListsReactions() *graphql.Field {
	return &graphql.Field{
		Type: types.ListsReactionsType,
		Args: graphql.FieldConfigArgument{
			"idReaction": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"idWriter": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"idProject": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[mutation] create lists reactions\n")
			log.WithFields(log.Fields{
				"idReaction": params.Args["idReaction"].(string),
				"idWriter":   params.Args["idWriter"].(string),
				"idProject":  params.Args["idProject"].(string),
			}).Debug("__ create list reactions\n")

			idReaction, _ := uuid.FromString(params.Args["idReaction"].(string))
			idUser, _ := uuid.FromString(params.Args["idWriter"].(string))
			idProject, _ := uuid.FromString(params.Args["idProject"].(string))

			listsReactions := &types.ListsReactions{
				ID_reaction: idReaction,
				ID_user:   idUser,
				ID_project:  idProject,
			}

			repository.Create(listsReactions)

			return listsReactions, nil
		},
	}
}
