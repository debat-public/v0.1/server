package types

import (
	"github.com/graphql-go/graphql"
	"github.com/satori/go.uuid"
	"github.com/jinzhu/gorm"
)

// Project type definition.
type Project struct {
	ID			uuid.UUID	`gorm:"type:uuid;primary_key;" db:"id" json:"id"`
	Title       string 		`db:"title" json:"title"`
	Description string 		`db:"description" json:"description"`
	CreatedAt   string 		`db:"created_at" json:"created_at"`
}

// ProjectType is the GraphQL schema for the user type.
var ProjectType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Project",
	Fields: graphql.Fields{
		"id":          &graphql.Field{Type: graphql.String},
		"title":       &graphql.Field{Type: graphql.String},
		"description": &graphql.Field{Type: graphql.String},
		"created_at":  &graphql.Field{Type: graphql.String},
	},
})

// BeforeCreate will set a UUID rather than numeric ID.
func (project *Project) BeforeCreate(scope *gorm.Scope) error {
	uuid := uuid.NewV4()
	return scope.SetColumn("ID", uuid)
}