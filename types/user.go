package types

import (
	"github.com/satori/go.uuid"
	"github.com/jinzhu/gorm"
	"github.com/graphql-go/graphql"
)

// User type definition.
type User struct {
	ID    	  uuid.UUID	`gorm:"type:uuid;primary_key;" db:"id" json:"id_admin"`
	Username  string `db:"username" json:"username"`
	Firstname string `db:"firstname" json:"firstname"`
	Lastname  string `db:"lastname" json:"lastname"`
	Email     string `db:"email" json:"email"`
	Password  string `db:"password" json:"password"`
	Job       string `db:"job" json:"job"`
	Bio       string `db:"bio" json:"bio"`
	Age       string `db:"age" json:"age"`
	CreatedAt string `db:"created_at" json:"created_at"`
}

// UserType is the GraphQL schema for the user type.
var UserType = graphql.NewObject(graphql.ObjectConfig{
	Name: "User",
	Fields: graphql.Fields{
		"id":         &graphql.Field{Type: graphql.String},
		"username":   &graphql.Field{Type: graphql.String},
		"firstname":  &graphql.Field{Type: graphql.String},
		"lastname":   &graphql.Field{Type: graphql.String},
		"email":      &graphql.Field{Type: graphql.String},
		"password":   &graphql.Field{Type: graphql.String},
		"job":        &graphql.Field{Type: graphql.String},
		"bio":        &graphql.Field{Type: graphql.String},
		"age":        &graphql.Field{Type: graphql.String},
		"roles": &graphql.Field{
			Type: graphql.NewList(RoleType),
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				var roles []Role

				// userID := params.Source.(User).ID
				// Implement logic to retrieve user associated roles from user id here.

				return roles, nil
			},
		},
	},
})

// BeforeCreate will set a UUID rather than numeric ID.
func (user *User) BeforeCreate(scope *gorm.Scope) error {
	uuid := uuid.NewV4()
	return scope.SetColumn("ID", uuid)
}

