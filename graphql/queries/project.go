package queries

import (
	log "github.com/sirupsen/logrus"

	"server/repository"
	"server/types"

	"github.com/satori/go.uuid"
	"github.com/graphql-go/graphql"
)

type ListDataProject struct {
	Project		types.Project
	User		types.User
	Block		[]types.Block
}

// ListDataProjectType is an output GraphQL schema for the project user block relation type.
var ListDataProjectType = graphql.NewObject(graphql.ObjectConfig{
	Name: "ListDataProject",
	Fields: graphql.Fields{
		"project": &graphql.Field{Type: types.ProjectType},
		"user":   &graphql.Field{Type: types.UserType},
		"block":   &graphql.Field{Type: graphql.NewList(types.BlockType)},
	},
})

// FindProject returns project from id.
func FindProject() *graphql.Field {
	return &graphql.Field{
		Type:        ListDataProjectType,
		Description: "Return Project from ID",
		Args: graphql.FieldConfigArgument{
			"id": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[query] Project \n")
			log.WithFields(log.Fields{
				"id": params.Args["id"].(string),
			}).Debug("__ Query Project\n")

			var project types.Project
			var listsblock []types.ListsBlocks
			var blocks []types.Block
			var user types.User
			var listsProject []types.ListsProject

			id, _ := uuid.FromString(params.Args["id"].(string))

			repository.Find(&project, id)

			repository.FindListBy(&listsProject, "id_project", id, -1, -1)

			repository.Find(&user, listsProject[0].ID_user)

			repository.FindListBy(&listsblock, "id_project", id, -1, -1)

			listIndex := getIdproject(listsblock)

			repository.FindByList(&blocks, listIndex)

			dataProject := &ListDataProject{
				Project : project,
				User : user,
				Block : blocks,
			}

			return dataProject, nil
		},
	}
}

// FindListProjects
func FindListProjects() *graphql.Field {
	return &graphql.Field{
		Type: graphql.NewList(types.ProjectType),
		Args: graphql.FieldConfigArgument{
			"offset": &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: 0,
			},
			"limit": &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: 0,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[query] Query ListProjects\n")
			log.WithFields(log.Fields{
				"offset": params.Args["offset"].(int),
				"limit":  params.Args["limit"].(int),
			}).Debug("__ Query ListProjects\n")

			var projects []types.Project

			repository.FindAll(&projects, params.Args["offset"].(int), params.Args["limit"].(int))

			return projects, nil
		},
	}
}

func getIdproject(source []types.ListsBlocks) ([]string) {
	new_tab := make([]string, len(source))

	for i, e := range source {
		new_tab[i] = e.ID_block.String()
	}

	return new_tab
}