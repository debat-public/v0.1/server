package security

import (
	log "github.com/sirupsen/logrus"
	"github.com/dgrijalva/jwt-go"
	"time"
	"server/config"
)

// GenerateToken generates a jwt token and assign a username to it's claims and return it
func GenerateToken(username string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username": username,
		"exp": time.Now().Add(time.Minute * 60).Unix(),
	})

	config := config.GetConfig()

	tokenString, err := token.SignedString([]byte(config.JWT.Secret))
	if err != nil {
		log.Debug(err)
		return "", err
	}

	return tokenString, nil
}

// ParseToken parses a jwt token and returns the username in it's claims
func ParseToken(tokenStr string) (string, error) {
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		return config.GetConfig(), nil
	})
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		username := claims["username"].(string)
		return username, nil
	} else {
		return "", err
	}
}