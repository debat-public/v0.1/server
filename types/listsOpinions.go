package types

import (
	"github.com/graphql-go/graphql"
	"github.com/satori/go.uuid"
)

// Lists Opinions type definition.
type ListsOpinions struct {
	ID_opinion	uuid.UUID    `gorm:"type:uuid" db:"id_opinion" json:"id_opinion"`
	ID_user	uuid.UUID    `gorm:"type:uuid" db:"id_user" json:"id_user"`
	ID_project	uuid.UUID    `gorm:"type:uuid" db:"id_project" json:"id_project"`
}

// ListsOpinionsType is the GraphQL schema for the list opinion type.
var ListsOpinionsType = graphql.NewObject(graphql.ObjectConfig{
	Name: "ListsOpinions",
	Fields: graphql.Fields{
		"id_opinion": &graphql.Field{Type: graphql.String},
		"id_user":  &graphql.Field{Type: graphql.String},
		"id_project": &graphql.Field{Type: graphql.String},
	},
})
