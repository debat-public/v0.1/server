package main

import (
	"net/http"
	"os"

	"server/cli"
	"server/graphql"
	"server/security"
	"server/utils"

	"github.com/graphql-go/handler"
	log "github.com/sirupsen/logrus"
)

func main() {

	utils.InitPostgresSQL()
	defer utils.DB.Close()
	if cli.ParseArguments(os.Args[1:]) == false {
		return
	}

	schema := graphql.InitGraphQL()

	httpHandler := handler.New(&handler.Config{
		Schema:   &schema,
		Pretty:   true,
		GraphiQL: true,
	})

	http.Handle("/", security.Handle(httpHandler))

	// http.Handle("/graphql", h)

	log.Info("ready: listening.... :8383")

	r := http.ListenAndServe(":8383", nil)
	log.Fatal(r)

	// Test PostgresSQL

	// TODO add ERROR LOG
}
