package mutations

import (
	"server/repository"
	"server/types"

	log "github.com/sirupsen/logrus"

	"github.com/graphql-go/graphql"
)

// CreateRole creates a new role and returns it.
func CreateRole() *graphql.Field {
	return &graphql.Field{
		Type: types.RoleType,
		Args: graphql.FieldConfigArgument{
			"name": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[mutation] create role\n")
			log.WithFields(log.Fields{
				"name": params.Args["name"].(string),
			}).Debug("__ create role\n")

			role := &types.Role{
				Name: params.Args["name"].(string),
			}

			repository.Create(role)

			return role, nil
		},
	}
}
