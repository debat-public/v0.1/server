#################################################################################################
#																								#
#						Makefile to supervise Server developpement								#
#																								#
#################################################################################################


# Examples :

# For local developpement

# > make run

# For Docker developpement

# > make dev
# > make dev_reload


dev: docker_run 

dev_reload: docker_run_without_build 


#############################################################################################################
# Global variable of the Makefile
#################################################################################################################

# Default variable for the log
log_level = debug

# Token for JWT authentification
JWT = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SiSH2-RElFFXOmd-kbzyxt5TVON0rgV7T7_lsd30Kn4"

YELLOW_START = "\033[33m"

YELLOW_END = "\033[0m"

#########################################################################################
#  GO command
#########################################################################################

# clean packages
tidy:
	go mod tidy


#  https://dpp.st/blog/golang-modules/
vendor:
	go mod vendor

# Build the application
build: vendor
	go build -mod vendor

# Start the application for developement
run: build
	./server --log_level $(log_level)

# Setup the database with the JSON
# Warning: to init_fixture in docker-compose use docker_init_fixtures
init_fixtures:
	./server --fixtures

# Do some unitest
test:
	go test server/utils

################################################################################
#   DOCKER
################################################################################

# Reload the container in docker-compose. See infra project
docker_compose_reload:
	docker-compose  -f ../infra/dev/docker-compose.yaml restart   server

# Build 
docker_build:
	docker build -t registry.gitlab.com/debat-public/server .

# Build and launch Docker with the app
docker_run: docker_stop docker_rm docker_build
	docker run -d -p 8383:8383 -v $(PWD):/go/src/app --name server_container registry.gitlab.com/debat-public/server:latest

docker_shell:
	docker exec -it server_container /bin/sh

docker_init_fixtures:
	docker exec -it server_container make init_fixtures

docker_run_without_build:  docker_stop docker_rm
	docker run -d -p 8383:8383 -v $(PWD):/go/src/app --name server_container registry.gitlab.com/debat-public/server:latest

# Stop container
docker_stop:
	docker stop server_container || true

# Remove container
docker_rm:
	docker rm -f server_container || true

# Stop all docker containers
docker_fstop:
	docker stop $(docker ps -a -q)

# Build and Push the image to the gitlab registry
docker_push: docker_build docker_login
	docker push registry.gitlab.com/debat-public/server

# Login docker with registry.gitlab.com
docker_login:
	docker login registry.gitlab.com


# Remove all docker containers and images from the machine (add -f to force delete)
docker_fclean:
	docker rm $(docker ps -a -q)
	docker rmi $(docker images -q)

#Logs
docker_log:
	docker logs -f server_container


#############################################################################################################
# Test GraphQL
#################################################################################################################

test_mutations:
	curl -X POST  -H 'Content-Type: application/json' -H "Accept: application/json" -H "Authorization: Bearer $(JWT)"  -d '{"query": "mutation { createUser(firstname: \"John\", lastname: \"Snow\") { id,firstname,lastname } }"}' http://localhost:8383

test_mutations_project:
	curl -X POST  -H 'Content-Type: application/json' -H "Accept: application/json" -H "Authorization: Bearer $(JWT)"  -d '{"query": "mutation { createProject(description: \"Tro Daccord\", title: \"Mon super Project\") {description,title } }"}' http://localhost:8383

test_mutations_opinon:
	curl -X POST  -H 'Content-Type: application/json' -H "Accept: application/json" -H "Authorization: Bearer $(JWT)"  -d '{"query": "mutation { createOpinion(description: \"Tro Daccord\", title: \"Ma super Reaction\") {description,title } }"}' http://localhost:8383

test_mutations_reaction:
	curl -X POST  -H 'Content-Type: application/json' -H "Accept: application/json" -H "Authorization: Bearer $(JWT)"  -d '{"query": "mutation { createReaction(title: \"SALUT\") { title } }"}' http://localhost:8383

test_mutations_lists_reactions:
	curl -X POST  -H 'Content-Type: application/json' -H "Accept: application/json" -H "Authorization: Bearer $(JWT)"  -d '{"query": "mutation { createListsReactions(id_reaction: 0, id_writer: 0, id_post: 0) { id_reaction, id_writer, id_post } }"}' http://localhost:8383

# make test_queries_user id=44
test_queries_user:
	curl  -X POST -H 'Content-Type: application/json' -H "Accept: application/json" -H "Authorization: Bearer $(JWT)" -d '{"query": "query { findUser(id:$(id)) { firstname, lastname } }"}' http://localhost:8383

test_mutations_lists_opinions:
	curl -X POST  -H 'Content-Type: application/json' -H "Accept: application/json" -H "Authorization: Bearer $(JWT)"  -d '{"query": "mutation { createListsOpinions(id_opinion: 0, id_writer: 0, id_post: 0) { id_opinion, id_writer, id_post } }"}' http://localhost:8383

test_queries_listProjects:
	curl  -X POST -H 'Content-Type: application/json' -H "Accept: application/json" -H "Authorization: Bearer $(JWT)" -d '{"query": "query { listProjects { title, description } }"}' http://localhost:8383

test_queries_listParamProjects:
	curl -X POST  -H 'Content-Type: application/json' -H "Accept: application/json" -H "Authorization: Bearer $(JWT)"  -d '{"query": "query { listParamProjects(offset: 5, limit: 5) {description,title } }"}' http://localhost:8383

# make test_queries_ListUsersBy by=user id=4
test_queries_ListUsersBy:
	curl -X POST  -H 'Content-Type: application/json' -H "Accept: application/json" -H "Authorization: Bearer $(JWT)"  -d '{"query": "query { findUserRoleBy(by: \"$(by)\", id : $(id)) {id_user } }"}' http://localhost:8383

# make test_queries_listUserProjectBy by=writer id=4
test_queries_listUserProjectBy:
	curl -X POST  -H 'Content-Type: application/json' -H "Accept: application/json" -H "Authorization: Bearer $(JWT)"  -d '{"query": "query { findListsProjectBy(by: \"$(by)\", id : $(id)) {id_writer, id_project } }"}' http://localhost:8383

test_queries_listReactions:
	curl  -X POST -H 'Content-Type: application/json' -H "Accept: application/json"  -d '{"query": "query { listReactions { title } }"}' http://localhost:8383

test_queries_listlistsReactions:
	curl  -X POST -H 'Content-Type: application/json' -H "Accept: application/json"  -d '{"query": "query { findListsReactionBy(by:\"$(by)\", id: $(id)) { id_reaction, id_writer, id_project } }"}' http://localhost:8383

test_queries_listOpinions:
	curl  -X POST -H 'Content-Type: application/json' -H "Accept: application/json"  -d '{"query": "query { listOpinions { title } }"}' http://localhost:8383

test_queries_listlistsopinions:
	curl  -X POST -H 'Content-Type: application/json' -H "Accept: application/json"  -d '{"query": "query { findListsOpinionBy(by:\"$(by)\", id: $(id)) { id_opinion, id_writer, id_project } }"}' http://localhost:8383

test_queries_without_jwt:
	curl  -X POST -H 'Content-Type: application/json' -H "Accept: application/json" -d '{"query": "query { listProjects { title, description } }"}' http://localhost:8383

