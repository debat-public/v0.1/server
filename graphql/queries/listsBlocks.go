package queries

import (
	log "github.com/sirupsen/logrus"

	"server/repository"
	"server/types"

	"github.com/satori/go.uuid"
	"github.com/graphql-go/graphql"
)

// FindListBlocks return list of block from a project
func FindListBlocks() *graphql.Field {
	return &graphql.Field{
		Type:        graphql.NewList(types.BlockType),
		Description: "return list block/projet",
		Args: graphql.FieldConfigArgument{
			"id_project": &graphql.ArgumentConfig{
				Type:         graphql.NewNonNull(graphql.String),
			},
			"offset": &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: -1,
			},
			"limit": &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: -1,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[query] FindListBlockBy\n")
			log.WithFields(log.Fields{
				"id":     params.Args["id_project"].(string),
				"offset": params.Args["offset"].(int),
				"limit":  params.Args["limit"].(int),
			}).Debug("__ Query ListsBlocBy\n")

			var ret []types.ListsBlocks
			var blocks []types.Block

			id, _ := uuid.FromString(params.Args["id_project"].(string))

			repository.FindListBy(&ret, "id_project", id, params.Args["offset"].(int), params.Args["limit"].(int))
			
			listIndex := getIdBlock(ret)

			repository.FindByList(&blocks, listIndex)

			return blocks, nil
		},
	}
}

func getIdBlock(source []types.ListsBlocks) ([]string) {
	new_tab := make([]string, len(source))

	for i, e := range source {
		new_tab[i] = e.ID_block.String()
	}

	return new_tab
}
